package main

import (
	"strconv"
  "net"
  "encoding/json"
  "fmt"
  "bufio"
  "os"
)

func sendResponse(conn *net.UDPConn, addr *net.UDPAddr, message []byte) {
    _,err := conn.WriteToUDP(message, addr)
    if err != nil {
        fmt.Printf("Couldn't send response %v", err)
    }
}


type MaintMessage struct {
  Type_ string
  Destination int
  Data string
}


func main(){
  MyAddr,err := net.ResolveUDPAddr("udp","127.0.0.1:50000")
  if err != nil{
    panic("cant resolve my address")
  }
  
  ServerConn, err := net.ListenUDP("udp", MyAddr)
  if err != nil{
    panic("cant listen to udp")
  }
  
  scanner := bufio.NewScanner(os.Stdin)
  basePort := 40000
  for {
    tmpMessage := MaintMessage{}
    fmt.Print("Enter message type: ")
    scanner.Scan()
    tmpMessage.Type_ = scanner.Text()
    fmt.Printf("%s\n", tmpMessage.Type_)
    
    fmt.Print("Enter message destination: ")
    scanner.Scan()
    tmp := scanner.Text()
    tmpMessage.Destination, _ = strconv.Atoi(tmp)
    fmt.Printf("%d\n", tmpMessage.Destination)
    fmt.Print("Enter message data: ")
    scanner.Scan()
    tmpMessage.Data = scanner.Text()
    
    fmt.Printf("%s\n", tmpMessage.Data)
    fmt.Print("Enter id of a recipient: ")
    scanner.Scan()
    tmp = scanner.Text()
    id, _ := strconv.Atoi(tmp)
    
    
    neigbourAddress, _ := net.ResolveUDPAddr("udp", "127.0.0.1:" + strconv.Itoa(basePort + id))
    b, _ := json.Marshal(tmpMessage)
    sendResponse(ServerConn, neigbourAddress, b)
    fmt.Println("")
    
  }
  
}