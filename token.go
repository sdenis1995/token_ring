package main

import (
	"strconv"
  "net"
  "time"
  "encoding/json"
  "fmt"
  "os"
  "sync"
)

type Graph map[Node]Node

func sendResponse(conn *net.UDPConn, addr *net.UDPAddr, message []byte) {
    _,err := conn.WriteToUDP(message, addr)
    if err != nil {
        fmt.Printf("Couldn't send response %v", err)
    }
}



type Message struct{
  Type_ string
  Sender int
  Origin int
  Destination int
  Data string
}


type MaintMessage struct {
  Type_ string
  Destination int
  Data string
}

type Node struct {
	id   int
	port int
  maint_port int
}
type conMap map[int]*net.UDPAddr

//global variables
var basePort int
var baseMaintPort int
var token_delay time.Duration = time.Millisecond * 100
var token_timeout time.Duration = time.Millisecond * 100
var globalMessageId int64 = 1

var wg sync.WaitGroup

func newNode(a int) Node {
	return Node{
		id:   a,
		port: a + basePort,
    maint_port: a + baseMaintPort}
}

func (n Node) String() string {
	return strconv.Itoa(n.id)
}

func (n Node) Port() int {
	return n.port
}

func (g Graph) addEdge(a, b Node) {
	g[a] = b
}

func (g Graph) Neighbors(id int) (Node, bool) {
	node := newNode(id)
	neighbour, ok := g[node]
	return neighbour, ok
}

func (g Graph) GetNode(id int) (Node, bool) {
	node := newNode(id)
	_, ok := g[node]
	return node, ok
}


func Generate(n, port int, maint_port int) Graph {
	if n <= 0 {
		panic("wrong number of nodes")
	}

	basePort = port
  baseMaintPort = maint_port
	g := make(Graph)

	for i := 1; i < n; i++ {
		g.addEdge(newNode(i - 1), newNode(i))
	}
  g.addEdge(newNode(n - 1), newNode(0))
	return g
}

// Acquire attempts to obtain a permit within the allotted time
func Acquire(wait_chan chan int, timeout time.Duration) bool {
    cancel := make(chan struct{}, 1)
    t := time.AfterFunc(timeout, func() {
        close(cancel)
    })
    defer t.Stop()
    select {
    case <- cancel:
       return false
    case <- wait_chan:
        return true
    }
}


func ClientOperate(me Node, neighbour Node){
  myId := me.id
  
  //myCurState := 1 //1 - normal, 2 - not received for long or lost token (for main proc only)
  
  //main port
  MyAddr,err := net.ResolveUDPAddr("udp","127.0.0.1:" + strconv.Itoa(me.port))
  if err != nil{
    panic("cant resolve my address")
  }
  
  ServerConn, err := net.ListenUDP("udp", MyAddr)
  if err != nil{
    panic("cant listen to udp")
  }
  
  //maintenance port
  MyAddrMaint,err := net.ResolveUDPAddr("udp","127.0.0.1:" + strconv.Itoa(me.maint_port))
  if err != nil{
    panic("cant resolve my address")
  }
  
  ServerConnMaint, err := net.ListenUDP("udp", MyAddrMaint)
  if err != nil{
    panic("cant listen to udp")
  }
  
  buf := make([]byte, 1024 * 500)
  
  neigbourAddress, err := net.ResolveUDPAddr("udp", "127.0.0.1:" + strconv.Itoa(neighbour.port))
  
  //start := time.Now()
  sendMessagePool := make([]Message, 0)
  haveToken := false
  if myId == 0 {
    haveToken = true
  }
  sync_chan_0 := make(chan int)
  sync_chan := make(chan int)
  lose_token := false
  
  
  // обработка принятых сообщений (обычных)
  go func(){
    for {
      n,_,err := ServerConn.ReadFromUDP(buf)
      if err != nil{
        panic("cant read")
      }
      
      tmpMessage := Message{}
      err_ := json.Unmarshal(buf[0:n], &tmpMessage)
      if !lose_token {
        if err_ == nil {
          if tmpMessage.Type_ == "send" {
            if myId == tmpMessage.Destination {
              fmt.Printf("node %d: received token from node %d with data from %d (data: '%s'), sending token to %d\n", myId, tmpMessage.Sender, tmpMessage.Origin, tmpMessage.Data, neighbour.id)
              tmpMessage.Type_ = "notification"
              tmpMessage.Destination = tmpMessage.Origin
              tmpMessage.Origin = myId
              tmpMessage.Sender = myId
              tmpMessage.Data = ""
              b, _ := json.Marshal(tmpMessage)
              time.Sleep(token_delay)
              sendResponse(ServerConn, neigbourAddress, b)
            }else{
              tmpMessage.Sender = myId
              b, _ := json.Marshal(tmpMessage)
              time.Sleep(token_delay)
              sendResponse(ServerConn, neigbourAddress, b)
            }
          }else if tmpMessage.Type_ == "notification" {
            if myId != tmpMessage.Destination {
              tmpMessage.Sender = myId
              b, _ := json.Marshal(tmpMessage)
              time.Sleep(token_delay)
              sendResponse(ServerConn, neigbourAddress, b)
            }else {
              fmt.Printf("node %d: received token from node %d with delivery confirmation from %d\n", myId, tmpMessage.Sender, tmpMessage.Origin)
              sync_chan <- 1
            }
          }else if tmpMessage.Type_ == "empty" {
            fmt.Printf("node %d: received empty token from node %d\n", myId, tmpMessage.Sender)
            haveToken = true
          }else if tmpMessage.Type_ == "new-token" {
            if myId == 0 {
              //master node
              haveToken = true
              fmt.Printf("node %d: received new-token message again, generating new token\n", myId)
            }else{
              if haveToken{
                sync_chan <- 1
              }
              haveToken = false
              fmt.Printf("node %d: received new-token message from node %d, resetting all processes\n", myId, tmpMessage.Sender)
              b, _ := json.Marshal(tmpMessage)
              time.Sleep(token_delay)
              sendResponse(ServerConn, neigbourAddress, b)
            }
            
          }
          if myId == 0{
            sync_chan_0 <- 1
          }
        }
      }else{
        lose_token = false
      }
    }
  }() 
  // обработка принятых сообщений (управляющих)
  go func(){
    for {
      n,_,err := ServerConnMaint.ReadFromUDP(buf)
      if err != nil{
        panic("cant read")
      }
      
      tmpMessage := MaintMessage{}
      err_ := json.Unmarshal(buf[0:n], &tmpMessage)
      if err_ == nil {
        fmt.Printf("node %d: received service message: %s\n", myId, buf[0:n])
        if tmpMessage.Type_ == "send" {
          tmp := Message{}
          tmp.Data = tmpMessage.Data
          tmp.Destination = tmpMessage.Destination
          tmp.Origin = myId
          tmp.Sender = myId
          tmp.Type_ = "send"
          sendMessagePool = append(sendMessagePool, tmp)
        }else if tmpMessage.Type_ == "drop" {
          lose_token = true
        }
      }
    }
  }()
  
  // реализация проверки потери токена, и просто при получении пустого токена
  go func(){
    for {
      if !haveToken {
        if myId == 0 {
          flag := Acquire(sync_chan_0, token_timeout)
          if !flag {
            confirmed := false
            for !confirmed {
              //lost token, need to generate new one
              fmt.Printf("node %d: token timeout, initializing process of creating new one\n", myId)
              tmpMessage := Message{}
              tmpMessage.Type_ = "new-token"
              tmpMessage.Data = ""
              tmpMessage.Origin = myId
              tmpMessage.Sender = myId
              tmpMessage.Destination = myId
              b, _ := json.Marshal(tmpMessage)
              sendResponse(ServerConn, neigbourAddress, b)
              //waiting to message to come back
              confirmed = Acquire(sync_chan_0, token_timeout)
              if haveToken && confirmed {
                confirmed = true
              }else {
                confirmed = false
              }
            }
          }
        }
      }else {
        //sending all the messages in the pool
        confirmed := false
        for index, element := range sendMessagePool{
           confirmed = false
           for !confirmed {
              b, _ := json.Marshal(element)
              sendResponse(ServerConn, neigbourAddress, b)
              //wait for the notification
              confirmed = Acquire(sync_chan, token_timeout)
              if myId == 0 {
                <- sync_chan_0
              }
              if !haveToken {
                //got new-token message
                break
              }
           }
           if !haveToken {
              sendMessagePool = sendMessagePool[index : ]
              break
           }
        }
        if haveToken {
          //send empty token to the neighbour
          tmpMessage := Message{}
          tmpMessage.Data = ""
          tmpMessage.Type_ = "empty"
          tmpMessage.Origin = myId
          tmpMessage.Sender = myId
          tmpMessage.Destination = neighbour.id
          b, _ := json.Marshal(tmpMessage)
          time.Sleep(token_delay)
          sendResponse(ServerConn, neigbourAddress, b)
          haveToken = false
          sendMessagePool = make([]Message, 0)
        }
      }
    }
  }()
  
}

func main(){
  node_count := 0
  if len(os.Args) > 1{
    n, err := strconv.Atoi(os.Args[1])
    if err != nil{
      panic(err)
    }else{
      node_count = n
    }
  }
  if len(os.Args) > 2{
    del, err := strconv.Atoi(os.Args[2])
    if err != nil{
      panic(err)
    }else{
      token_delay = time.Millisecond * time.Duration(del)
    }
  }
  token_timeout = token_delay * time.Duration(float64(node_count) * 2.5)
	config_graph := Generate(node_count, 30000, 40000)
  
  //count := len(config_graph)
  
  for k, _ := range config_graph{
    neighbours, ok := config_graph.Neighbors(k.id)
    if ok{
      go ClientOperate(k, neighbours)
    }else{
      panic("Error : no neighbours of a node")
    }
  }
  wg.Add(1)
  wg.Wait()
}
